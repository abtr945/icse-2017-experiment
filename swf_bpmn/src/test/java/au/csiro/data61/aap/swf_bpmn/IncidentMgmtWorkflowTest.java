package au.csiro.data61.aap.swf_bpmn;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.amazonaws.services.simpleworkflow.flow.junit.FlowBlockJUnit4ClassRunner;
import com.amazonaws.services.simpleworkflow.flow.junit.WorkflowTest;

@RunWith(FlowBlockJUnit4ClassRunner.class)
public class IncidentMgmtWorkflowTest {

	@Rule
    public WorkflowTest workflowTest = new WorkflowTest();

    private IncidentMgmtWorkflowClientFactory workflowFactory = new IncidentMgmtWorkflowClientFactoryImpl();

    @Before
    public void setUp() throws Exception {
    	// Register activity implementation to be used during test run
        IncidentMgmtActivities activities = new IncidentMgmtActivitiesImpl();
        workflowTest.addActivitiesImplementation(activities);
        workflowTest.addWorkflowImplementationType(IncidentMgmtWorkflowImpl.class);
    }
    
    @Test
    public void testIncidentMgmtWorkflowFirstXorShort() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test
    public void testIncidentMgmtWorkflowSecondXorShort() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Provide_feedback_for_account_manager", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test
    public void testIncidentMgmtWorkflowThirdXorShort() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Ask_2nd_level_support", 
        				"Provide_feedback_for_1st_level_support", 
        				"Provide_feedback_for_account_manager", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test
    public void testIncidentMgmtWorkflowThirdXorLong() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Ask_2nd_level_support", 
        				"Ask_developer", 
        				"Provide_feedback_for_2nd_level_support", 
        				"Provide_feedback_for_1st_level_support", 
        				"Provide_feedback_for_account_manager", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test(expected = Exception.class)
    public void testIncidentMgmtWorkflowUnfitWrongTaskOrder() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem",  
        				"Explain_solution"), 
        		runId);
    }
    
    @Test(expected = Exception.class)
    public void testIncidentMgmtWorkflowUnfitFirstXorViolated() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test(expected = Exception.class)
    public void testIncidentMgmtWorkflowUnfitSecondXorViolated() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Ask_2nd_level_support", 
        				"Provide_feedback_for_account_manager", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Test(expected = Exception.class)
    public void testIncidentMgmtWorkflowUnfitThirdXorViolated() {
    	// Start a new workflow process
        IncidentMgmtWorkflowClient workflowClient = workflowFactory.getClient();
        workflowClient.start();
        // Wait for workflow to start, then send signals to progress the workflow
        Promise<String> runId = workflowClient.getRunId();
        sendSignals(workflowClient, 
        		Arrays.asList(
        				"Customer_Has_a_Problem", 
        				"Get_problem_description", 
        				"Ask_1st_level_support", 
        				"Ask_2nd_level_support", 
        				"Ask_developer", 
        				"Provide_feedback_for_1st_level_support", 
        				"Provide_feedback_for_account_manager", 
        				"Explain_solution"), 
        		runId);
    }
    
    @Asynchronous
	private void sendSignals(
			IncidentMgmtWorkflowClient workflowClient, 
			List<String> signals, 
			Promise<?>... waitFor) {
    	Iterator<String> it = signals.iterator();
    	while (it.hasNext()) {
    		String signal = it.next();
    		switch (signal) {
    		case "Customer_Has_a_Problem":
    			workflowClient.Customer_Has_a_Problem();
    			break;
    		case "Get_problem_description":
    			workflowClient.Get_problem_description();
    			break;
    		case "Ask_1st_level_support":
    			workflowClient.Ask_1st_level_support();
    			break;
    		case "Ask_2nd_level_support":
    			workflowClient.Ask_2nd_level_support();
    			break;
    		case "Ask_developer":
    			workflowClient.Ask_developer();
    			break;
    		case "Provide_feedback_for_2nd_level_support":
    			workflowClient.Provide_feedback_for_2nd_level_support();
    			break;
    		case "Provide_feedback_for_1st_level_support":
    			workflowClient.Provide_feedback_for_1st_level_support();
    			break;
    		case "Provide_feedback_for_account_manager":
    			workflowClient.Provide_feedback_for_account_manager();
    			break;
    		case "Explain_solution":
    			workflowClient.Explain_solution();
    			break;
    		default:
    			throw new RuntimeException("Invalid signal.");
    		}
    	}
	}
    
}
