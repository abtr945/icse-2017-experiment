package au.csiro.data61.aap.swf_bpmn;

import java.util.Properties;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflowClient;
import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;
import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;

public class IncidentMgmtWorker {
	private static final String APP_CONFIG_FILE = "application.properties";
	
	public static void main(String[] args) throws Exception {
		// Load AWS configuration from config file
		Properties prop = new Properties();
		prop.load(IncidentMgmtWorker.class.getClassLoader().getResourceAsStream(APP_CONFIG_FILE));

		// Initialize Amazon SWF Client object and SWF domain
		ClientConfiguration config = new ClientConfiguration().withSocketTimeout(70 * 1000);
		AWSCredentialsProvider awsCredentialsProvider = new DefaultAWSCredentialsProviderChain();
		AmazonSimpleWorkflow service = new AmazonSimpleWorkflowClient(awsCredentialsProvider, config);
		service.setEndpoint(prop.getProperty("amazon.swf.endpoint"));

		String domain = prop.getProperty("amazon.swf.domain");
		
		// The task list to poll for Activity and Workflow tasks
		String taskListToPoll = "IncidentMgmtList";

		// Start the Activity Worker
		ActivityWorker aw = new ActivityWorker(service, domain, taskListToPoll);
		aw.addActivitiesImplementation(new IncidentMgmtActivitiesImpl());
		aw.start();

		// Start the Workflow Worker
		WorkflowWorker wfw = new WorkflowWorker(service, domain, taskListToPoll);
		wfw.addWorkflowImplementationType(IncidentMgmtWorkflowImpl.class);
		wfw.start();
	}
}
