package au.csiro.data61.aap.swf_bpmn;

import com.amazonaws.services.simpleworkflow.flow.annotations.Execute;
import com.amazonaws.services.simpleworkflow.flow.annotations.GetState;
import com.amazonaws.services.simpleworkflow.flow.annotations.Signal;
import com.amazonaws.services.simpleworkflow.flow.annotations.Workflow;
import com.amazonaws.services.simpleworkflow.flow.annotations.WorkflowRegistrationOptions;

@Workflow
@WorkflowRegistrationOptions(defaultExecutionStartToCloseTimeoutSeconds = 7200)
public interface IncidentMgmtWorkflow {
	
	/**
	 * Entrypoint of the workflow representing a business process instance.
	 */
	@Execute(version = "1.0")
	public void start();
	
	/**
	 * Get the current state of the process instance execution
	 * (i.e. the tasks/logic gates activation mapping).
	 * 
	 * @return String representation of the activation mapping.
	 */
	@GetState
	public String getState();
	
	/**
	 * SWF Signal API: Customer_Has_a_Problem
	 */
	@Signal
    public void Customer_Has_a_Problem();
    
	/**
	 * SWF Signal API: Get_problem_description
	 */
	@Signal
    public void Get_problem_description();
    
	/**
	 * SWF Signal API: Ask_1st_level_support
	 */
	@Signal
    public void Ask_1st_level_support();
    
	/**
	 * SWF Signal API: Ask_2nd_level_support
	 */
	@Signal
    public void Ask_2nd_level_support();
    
	/**
	 * SWF Signal API: Ask_developer
	 */
	@Signal
    public void Ask_developer();
    
	/**
	 * SWF Signal API: Provide_feedback_for_2nd_level_support
	 */
	@Signal
    public void Provide_feedback_for_2nd_level_support();
    
	/**
	 * SWF Signal API: Provide_feedback_for_1st_level_support
	 */
	@Signal
    public void Provide_feedback_for_1st_level_support();
    
	/**
	 * SWF Signal API: Provide_feedback_for_account_manager
	 */
	@Signal
    public void Provide_feedback_for_account_manager();
    
	/**
	 * SWF Signal API: Explain_solution
	 */
	@Signal
    public void Explain_solution();
	
}
