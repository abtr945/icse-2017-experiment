package au.csiro.data61.aap.swf_bpmn;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.amazonaws.services.simpleworkflow.flow.core.Settable;

public class IncidentMgmtWorkflowImpl implements IncidentMgmtWorkflow {
	
	private IncidentMgmtActivitiesClient activities = new IncidentMgmtActivitiesClientImpl();
	
	private Map<String, Promise<Void>> taskCompletionPromises;
	
	private Map<String, Boolean> isActivated;
	private Settable<Void> endStateReachedSignal;

	@Override
	public void start() {
		// Initialize the mappings containing the workflow state
		isActivated = new HashMap<String, Boolean>();
		taskCompletionPromises = new HashMap<String, Promise<Void>>();
		
		// Initialize the signal for process end state, which is initially unset.
		endStateReachedSignal = new Settable<Void>();
		
		// Define all tasks and logic gates in the activation mapping.
		// Activate the first task.
		isActivated.put("Customer_Has_a_Problem", true);
		isActivated.put("Get_problem_description", false);
		isActivated.put("XOR_split_1", false);
		isActivated.put("Ask_1st_level_support", false);
		isActivated.put("XOR_split_2", false);
		isActivated.put("Ask_2nd_level_support", false);
		isActivated.put("XOR_split_3", false);
		isActivated.put("Ask_developer", false);
		isActivated.put("Provide_feedback_for_2nd_level_support", false);
		isActivated.put("XOR_join_3", false);
		isActivated.put("Provide_feedback_for_1st_level_support", false);
		isActivated.put("XOR_join_2", false);
		isActivated.put("Provide_feedback_for_account_manager", false);
		isActivated.put("XOR_join_1", false);
		isActivated.put("Explain_solution", false);
		
		// Schedule the business process End State task,
		// which will only be executed when the EndStateReached signal is triggered.
		end(endStateReachedSignal);
	}

	@Override
	public String getState() {
		return isActivated.toString();
	}

	@Override
	public void Customer_Has_a_Problem() {
		if (isActivated.get("Customer_Has_a_Problem")) {
			// Schedule the task execution
			taskCompletionPromises.put("Customer_Has_a_Problem", 
					activities.Customer_Has_a_Problem());
			// Deactivate this task and activate the child node of this task.
			isActivated.put("Customer_Has_a_Problem", false);
			isActivated.put("Get_problem_description", true);
		} else {
			throw new RuntimeException(
					"[Customer_Has_a_Problem] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Get_problem_description() {
		if (isActivated.get("Get_problem_description")) {
			// Schedule the task execution
			taskCompletionPromises.put("Get_problem_description", 
					activities.Get_problem_description(
							taskCompletionPromises.get("Customer_Has_a_Problem")));
			// Deactivate this task and activate the child node of this task.
			isActivated.put("Get_problem_description", false);
			isActivated.put("XOR_split_1", true);
		} else {
			throw new RuntimeException(
					"[Get_problem_description] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Ask_1st_level_support() {
		// Immediate child node of an XOR_split gate. Need to check activation of parent gate.
		if (isActivated.get("XOR_split_1")) {
			// Schedule the task execution
			taskCompletionPromises.put("Ask_1st_level_support", 
					activities.Ask_1st_level_support( 
							taskCompletionPromises.get("Get_problem_description")));
			// XOR: Deactivate the parent gate and activate the child node of this task.
			isActivated.put("XOR_split_1", false);
			isActivated.put("XOR_split_2", true);
		} else {
			throw new RuntimeException(
					"[Ask_1st_level_support] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Ask_2nd_level_support() {
		// Immediate child node of an XOR_split gate. Need to check activation of parent gate.
		if (isActivated.get("XOR_split_2")) {
			// Schedule the task execution
			taskCompletionPromises.put("Ask_2nd_level_support", 
					activities.Ask_2nd_level_support(
							taskCompletionPromises.get("Ask_1st_level_support")));
			// XOR: Deactivate the parent gate and activate the child node of this task.
			isActivated.put("XOR_split_2", false);
			isActivated.put("XOR_split_3", true);
		} else {
			throw new RuntimeException("[Ask_2nd_level_support] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Ask_developer() {
		// Immediate child node of an XOR_split gate. Need to check activation of parent gate.
		if (isActivated.get("XOR_split_3")) {
			// Schedule the task execution
			taskCompletionPromises.put("Ask_developer", 
					activities.Ask_developer(
							taskCompletionPromises.get("Ask_2nd_level_support")));
			// XOR: Deactivate the parent gate and activate the child node of this task.
			isActivated.put("XOR_split_3", false);
			isActivated.put("Provide_feedback_for_2nd_level_support", true);
		} else {
			throw new RuntimeException("[Ask_developer] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Provide_feedback_for_2nd_level_support() {
		// XOR_join gate after this. Checking activation of child gate.
		if (isActivated.get("XOR_join_3")) {
			throw new RuntimeException(
					"[Provide_feedback_for_2nd_level_support] Non-conforming: XOR_join_3 already active");
		}
		
		if (isActivated.get("Provide_feedback_for_2nd_level_support")) {
			// Schedule the task execution
			taskCompletionPromises.put("Provide_feedback_for_2nd_level_support", 
					activities.Provide_feedback_for_2nd_level_support( 
							taskCompletionPromises.get("Ask_developer")));
			// Deactivate this task and activate the child node of this task.
			isActivated.put("Provide_feedback_for_2nd_level_support", false);
			isActivated.put("XOR_join_3", true);
		} else {
			throw new RuntimeException(
					"[Provide_feedback_for_2nd_level_support] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Provide_feedback_for_1st_level_support() {
		// XOR_join gate after this. Checking activation of child gate.
		if (isActivated.get("XOR_join_2")) {
			throw new RuntimeException(
					"[Provide_feedback_for_1st_level_support] Non-conforming: XOR_join_2 already active");
		}
		
		// Immediate child node of an XOR_join gate. Checking activation of parent gate.
		if (isActivated.get("XOR_join_3")) {
			taskCompletionPromises.put("Provide_feedback_for_1st_level_support", 
					activities.Provide_feedback_for_1st_level_support(
							taskCompletionPromises.get("Provide_feedback_for_2nd_level_support")));
			isActivated.put("XOR_join_3", false);
			isActivated.put("XOR_join_2", true);
		} 
		// NOTE: since parent XOR branch has empty branch, need to check if empty branch is followed.
		else if (isActivated.get("XOR_split_3")) {
			taskCompletionPromises.put("Provide_feedback_for_1st_level_support", 
					activities.Provide_feedback_for_1st_level_support(
							taskCompletionPromises.get("Ask_2nd_level_support")));
			isActivated.put("XOR_split_3", false);
			isActivated.put("XOR_join_2", true);
		} else {
			throw new RuntimeException(
					"[Provide_feedback_for_1st_level_support] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Provide_feedback_for_account_manager() {
		// XOR_join gate after this. Checking activation of child gate.
		if (isActivated.get("XOR_join_1")) {
			throw new RuntimeException(
					"[Provide_feedback_for_account_manager] Non-conforming: XOR_join_1 already active");
		}

		// Immediate child node of an XOR_join gate. Checking activation of parent gate.
		if (isActivated.get("XOR_join_2")) {
			taskCompletionPromises.put("Provide_feedback_for_account_manager", 
					activities.Provide_feedback_for_account_manager(
							taskCompletionPromises.get("Provide_feedback_for_1st_level_support")));
			isActivated.put("XOR_join_2", false);
			isActivated.put("XOR_join_1", true);
		}
		// NOTE: since parent XOR branch has empty branch, need to check if empty branch is followed.
		else if (isActivated.get("XOR_split_2")) {
			taskCompletionPromises.put("Provide_feedback_for_account_manager", 
					activities.Provide_feedback_for_account_manager(
							taskCompletionPromises.get("Ask_1st_level_support")));
			isActivated.put("XOR_split_2", false);
			isActivated.put("XOR_join_1", true);
		} else {
			throw new RuntimeException(
					"[Provide_feedback_for_account_manager] Non-conforming task registration API call received.");
		}
	}

	@Override
	public void Explain_solution() {
		// Immediate child node of an XOR_join gate. Checking activation of parent gate.
		if (isActivated.get("XOR_join_1")) {
			taskCompletionPromises.put("Explain_solution",
					activities.Explain_solution(
							taskCompletionPromises.get("Provide_feedback_for_account_manager")));
			isActivated.put("XOR_join_1", false);
		}
		// NOTE: since parent XOR branch has empty branch, need to check if empty branch is followed.
		else if (isActivated.get("XOR_split_1")) {
			taskCompletionPromises.put("Explain_solution",
					activities.Explain_solution(
							taskCompletionPromises.get("Get_problem_description")));
			isActivated.put("XOR_split_1", false);
		} else {
			throw new RuntimeException(
					"[Explain_solution] Non-conforming task registration API call received.");
		}
		
		// This is the last task of the workflow. Signal the process end state when this task is completed.
		signalProcessEndState(taskCompletionPromises.get("Explain_solution"));
	}
	
	@Asynchronous
	private void signalProcessEndState(Promise<?>... waitFor) {
		endStateReachedSignal.set(null);
	}
	
	@Asynchronous
	private void end(Promise<?>... waitFor) {
		//System.out.println("END");
	}
	
}
