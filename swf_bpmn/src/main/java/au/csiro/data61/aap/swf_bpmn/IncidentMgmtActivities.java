package au.csiro.data61.aap.swf_bpmn;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

@Activities(version = "1.0")
@ActivityRegistrationOptions(defaultTaskScheduleToStartTimeoutSeconds = 300, defaultTaskStartToCloseTimeoutSeconds = 10)
public interface IncidentMgmtActivities {
	
	/**
	 * SWF Activity task: Customer_Has_a_Problem
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Customer_Has_a_Problem();
    
    /**
	 * SWF Activity task: Get_problem_description
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Get_problem_description();
    
    /**
	 * SWF Activity task: Ask_1st_level_support
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Ask_1st_level_support();
    
    /**
	 * SWF Activity task: Ask_2nd_level_support
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Ask_2nd_level_support();
    
    /**
	 * SWF Activity task: Ask_developer
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Ask_developer();
    
    /**
	 * SWF Activity task: Provide_feedback_for_2nd_level_support
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Provide_feedback_for_2nd_level_support();
    
    /**
	 * SWF Activity task: Provide_feedback_for_1st_level_support
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Provide_feedback_for_1st_level_support();
    
    /**
	 * SWF Activity task: Provide_feedback_for_account_manager
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Provide_feedback_for_account_manager();
    
    /**
	 * SWF Activity task: Explain_solution
	 * Notify external actors of next available task(s) in the business process.
	 */
    public void Explain_solution();
    
}
