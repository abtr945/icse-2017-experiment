package au.csiro.data61.aap.swf_bpmn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class IncidentMgmtActivitiesImpl implements IncidentMgmtActivities {
	
	private String notificationApiEndpoint;

	public IncidentMgmtActivitiesImpl() throws IOException {
		// Load configuration from config file
		Properties prop = new Properties();
		prop.load(IncidentMgmtWorker.class.getClassLoader().getResourceAsStream("application.properties"));
		notificationApiEndpoint = "http://" + prop.getProperty("notification.api.host") + 
				":" + prop.getProperty("notification.api.port") + "/notification";
	}
	
	@Override
	public void Customer_Has_a_Problem() {
		notificationApiCall();
	}

	@Override
	public void Get_problem_description() {
		notificationApiCall();
	}

	@Override
	public void Ask_1st_level_support() {
		notificationApiCall();
	}

	@Override
	public void Ask_2nd_level_support() {
		notificationApiCall();
	}
	
	@Override
	public void Ask_developer() {
		notificationApiCall();
	}

	@Override
	public void Provide_feedback_for_2nd_level_support() {
		notificationApiCall();
	}
	
	@Override
	public void Provide_feedback_for_1st_level_support() {
		notificationApiCall();
	}

	@Override
	public void Provide_feedback_for_account_manager() {
		notificationApiCall();
	}

	@Override
	public void Explain_solution() {
		notificationApiCall();
	}
	
	/**
	 * Dummy implementation of notification activity.
	 * Call an external API to simulate a notification.
	 */
	private void notificationApiCall() {
		// Send a GET request to the notification API endpoint
		try {
			URL obj = new URL(notificationApiEndpoint);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			//System.out.println("HTTP " + responseCode + ": " + response.toString());
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

}
