icse-2017-experiment
====================

Repository content
------------------

* `swf_bpmn`: Implementation of "Incident Management" business process on Amazon Simple Workflow Service (Amazon SWF), using AWS Flow Framework for Java.
  * To build: `mvn package`
* `bpmn_logs`: Log trace of Incident Management business process used in the experiment.
